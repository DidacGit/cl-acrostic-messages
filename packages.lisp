;;;; Package definitions of the library.

;;; This file contains only the package definitions of the library. It should be
;;; the first one to be loaded.

(in-package :common-lisp-user)

(defpackage :didac.acrostic.core
  (:use :common-lisp)
  (:export :message-present-p))

(defpackage :didac.acrostic.showcase
  (:use :common-lisp :didac.acrostic.core)
  (:export :showcase))
