;;;; Check for hidden acrostic messages in a magazine.

;;; Provides a function that checks whether an acrostic message is hidden in a
;;; magazine file, either in the beginnings or endings of the words.

(in-package :didac.acrostic.core)

(defun process-message (message)
  "Process the MESSAGE into a hash table."
  
  ;; Eliminate all non-alphabetic characters form the string.
  (setf message (remove-if-not #'alpha-char-p message))
  (if (zerop (length message))
      (error "The message doesn't have any alphabetic characters.")
      
      ;; Collect the amount of each letter in a hash table.
      ;; Use the case-insensitive test function for its keys: string characters.
      (let ((table (make-hash-table :test #'equalp)))
	(loop for c across message do
	  (if (gethash c table)
	      (incf (gethash c table))
	      (setf (gethash c table) 1)))
	table)))

(defun dec-or-remove-hash-key (key table)
  "Decrease by 1 KEY's value or, if it will be 0, remove it, from TABLE."
  (if (hash-table-p table)
      (let ((value (gethash key table)))
	(when value (let ((new-value (1- value)))
		      (if (zerop new-value)
			  (remhash key table)
			  (setf (gethash key table) new-value)))))
      (error "The passed TABLE is not a hash table.")))

(defun message-present-p (message magazine)
  "Check for the presence of MESSAGE in MAGAZINE."
  (with-open-file (in magazine :if-does-not-exist nil)
    (if in
	;; Keep two tables with the messages characters: one for checking the
	;; message in the first letters of words and another for the last
	;; letters of words. Every time a letter matches, remove it from the
	;; table.

	;; If, before finishing reading the whole magazine, one of the two
	;; tables is empty, return T. Else return NIL.
	(let ((first-table (process-message message))
	      (last-table (process-message message))
	      (prev-char nil))
	  
	  ;; Read the magazine char by char.
	  (loop for char = (read-char in nil)
		while char do
		  
		  ;; The current character is a start if it's alphabetic and the
		  ;; previous character isn't or is NIL (beginning of file).
		  (when (and (alpha-char-p char)
			     (or (null prev-char)
				 (not (alpha-char-p prev-char))))
		    (dec-or-remove-hash-key char first-table))
		  
		  ;; The previous character is an end if it's alphabetic and the
		  ;; current character isn't.
		  (when (and prev-char (alpha-char-p prev-char)
			     (not (alpha-char-p char)))
		    (dec-or-remove-hash-key prev-char last-table))

		  ;; If either of the tables is empty, the message was found.
		  (if (or (= 0 (hash-table-count first-table))
			  (= 0 (hash-table-count last-table)))
		      (return-from message-present-p t))
		  
		  (setf prev-char char))
	  ;; If the message was not found, return NIL.
	  nil)
	(error (concatenate 'string "Could not open the file: '" magazine "'.")))))
