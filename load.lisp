;;;; Load the libraries of the repository.

;;; First load the package definitions and then the packages themselves. It's
;;; good practice to have only one IN-PACKAGE form per file, as the first form.

(load "packages.lisp")
(load "core.lisp")
(load "showcase.lisp")
