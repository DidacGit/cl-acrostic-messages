;;;; Showcase the library's functionality.

;;; Provides a simple function to showcase some examples of the library's
;;; functionality.

(in-package :didac.acrostic.showcase)

(defun showcase-magazine (magazine &rest messages)
  "Check the presence of MESSAGES in MAGAZINE, displaying the results."
  (format t "Testing the presence of these messages in '~a':~%" magazine)
  (handler-case
      (dolist (m messages)
	(if (message-present-p m magazine)
	    (format t "'~a'~45tIS in the magazine.~%" m)
	    (format t "'~a'~45tis NOT in the magazine.~%" m)))
    (error (e)
      (format t "Error while trying check the messages of magazine. ~a" e))))

(defun showcase ()
  "Showcase some examples of the functionality of the library."
  (format t "Examples of messages in two magazines:~%~%")
  (showcase-magazine "magazines/dance.txt"
		     "Hi Bob"
		     "Run"
		     "Hi Barb"
		     "I ran")
  (format t "~%")
  (showcase-magazine "magazines/lorem-ipsum.txt"
		     "No updates"
		     "The terrorists are close"
		     "The terrorist's base has been occupied"
		     "The conflict is over"
		     "The conflict will go on indefinitely"))
